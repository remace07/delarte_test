# Licence: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of [`delarte`](https://git.afpy.org/fcode/delarte.git)"""CLI arguments related module."""

"""Unit test for command-line args parser."""

from unittest import TestCase, mock

import argparse

from src.delarte.cli import Parser


class TestParser(TestCase):
    """Tests for args parser."""

    def setUp(self):
        """Create a CLI Parser."""
        self.parser = Parser()

    def tearDown(self):
        """Delete the CLI Parser."""
        self.parser = None

    def test_args_parse(self):
        """Test this parser gets the arguments from CLI."""
        args = vars(
            self.parser.parse_args(
                [
                    "https://www.arte.tv/en/videos/104001-000-A/clint-eastwood/",
                    "VOF-STMF",
                    "216p",
                ],
            )
        )
        self.assertEqual(
            args,
            {
                "version": "VOF-STMF",
                "resolution": "216p",
                "url": "https://www.arte.tv/en/videos/104001-000-A/clint-eastwood/",
            },
        )

    @mock.patch(
        "argparse.ArgumentParser.parse_args",
        return_value=argparse.Namespace(
            url="https://www.arte.tv/en/videos/104001-000-A/clint-eastwood/",
            version="VOF-STMF",
            resolution="216p",
        ),
    )
    def test_get_args_as_list(self, *mock_args):
        """Test the return method for listing arguments."""
        args = self.parser.get_args_as_list()
        self.assertEqual(
            args,
            [
                "https://www.arte.tv/en/videos/104001-000-A/clint-eastwood/",
                "VOF-STMF",
                "216p",
            ],
        )
