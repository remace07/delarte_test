# Licence: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of [`delarte`](https://git.afpy.org/fcode/delarte.git)

"""Provide contexted based file naming utility."""


def build_file_base_name(config):
    """Create a base file name from config metadata."""
    return config["attributes"]["metadata"]["title"].replace("/", "-")
