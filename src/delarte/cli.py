# Licence: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of [`delarte`](https://git.afpy.org/fcode/delarte.git)"""CLI arguments related module."""

"""
usage: delarte [-h|--help]                         - print this message
or: delarte program_page_url                       - show available versions
or: delarte program_page_url version               - show available video resolutions
or: delarte program_page_url version resolution    - download the given video
"""

import argparse


class Parser(argparse.ArgumentParser):
    """Parser responsible for parsing CLI arguments."""

    def __init__(self):
        """Generate a parser."""
        super().__init__(
            description="downloads Arte's videos with subtitles",
            epilog=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter,
        )
        self.add_argument(
            "url",
            help="url of Arte movie's webpage",
            action="store",
            type=str,
            nargs="?",
        )
        self.add_argument(
            "version",
            help="one of the language code proposed by Arte",
            action="store",
            type=str,
            nargs="?",
        )
        self.add_argument(
            "resolution",
            help="video resolution",
            action="store",
            type=str,
            nargs="?",
        )

    def get_args_as_list(self):
        """Get arguments from CLI as a list.

        Returns:
            List: ordered list of arguments, None removed
        """
        args_namespace = self.parse_args()
        args_list = [
            args_namespace.url,
            args_namespace.version,
            args_namespace.resolution,
        ]
        return [arg for arg in args_list if arg is not None]
