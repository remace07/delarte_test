# Licence: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of [`delarte`](https://git.afpy.org/fcode/delarte.git)

"""Provide ArteTV website utilities."""

from urllib.parse import urlparse

LANGUAGES = ["fr", "de", "en", "es", "pl", "it"]


def parse_url(program_page_url):
    """Parse ArteTV web URL into UI language and program ID."""
    url = urlparse(program_page_url)
    if url.hostname != "www.arte.tv":
        raise ValueError("not an ArteTV url")

    program_page_path = url.path.split("/")[1:]

    lang = program_page_path.pop(0)

    if lang not in LANGUAGES:
        raise ValueError(f"invalid url language code: {lang}")

    if program_page_path.pop(0) != "videos":
        raise ValueError("invalid ArteTV url")

    program_id = program_page_path.pop(0)

    return lang, program_id
