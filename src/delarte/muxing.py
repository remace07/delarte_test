# Licence: GNU AGPL v3: http://www.gnu.org/licenses/
# This file is part of [`delarte`](https://git.afpy.org/fcode/delarte.git)

"""Provide media muxing utilities."""

import subprocess


def mux(inputs, file_base_name, progress):
    """Build FFMPEG args."""
    video_input, audio_track, subtitles_track = inputs

    audio_lang, audio_input = audio_track
    if subtitles_track:
        subtitles_lang, subtitles_input = subtitles_track

    cmd = ["ffmpeg", "-hide_banner"]
    cmd.extend(["-i", video_input])
    cmd.extend(["-i", audio_input])
    if subtitles_track:
        cmd.extend(["-i", subtitles_input])

    cmd.extend(["-c:v", "copy"])
    cmd.extend(["-c:a", "copy"])
    if subtitles_track:
        cmd.extend(["-c:s", "copy"])

    cmd.extend(["-bsf:a", "aac_adtstoasc"])
    cmd.extend(["-metadata:s:a:0", f"language={audio_lang}"])

    if subtitles_track:
        cmd.extend(["-metadata:s:s:0", f"language={subtitles_lang}"])
        cmd.extend(["-disposition:s:0", "default"])

    cmd.append(f"{file_base_name}.mkv")

    subprocess.run(cmd)
